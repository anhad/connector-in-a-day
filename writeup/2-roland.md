Today we tried to cut a set of fingers out pf thin copper stock using the roland cnc. We encountered many obstacles.

We followed the traditional cnc toolpathing workflow and tried to find the smallest endmill in the lab, which ended up being 0.010", or 0.254mm. This was barely small enough to fit between the fingers.

The first isse was that thjis endmill was extremely short, being designed for just cutting the thin foil laminate on copper clad fr4, so it was not really up to the task of slotting through an entire copper sheet. 

Finally, fixturing the copper sheet was difficult because we needed a way to prevent it from vibrating due to cutting forces, and possibly breaking the delicate end mill. We ended up using the "blue tape trick" where we put painter's tape on a small square of copper, put another piece on the bed and then superglued the two taped faces together. This held the stock well but did not leave any room underneath for the endmill to pierce through the bottom of the copper, and there was no spoilboard underneath to mill into. We hoped to mill most of the way through the copper and break the rest by hand. 

Unfortunately at this point we broke the endmill trying to use the ATC, and at this point (since we could only find one in the lab) we decided that the various drawbacks we encountered so far meant that CNC machining was not the correct fabrication technology for the connector leadframes. 
