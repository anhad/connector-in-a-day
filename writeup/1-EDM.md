Today we tried to make some test cuts to see if it was possible to make the leadframes that are stamped in the Molex connectors using EDM. 

I designed a simple finger pattern with varying width fingers and we tried to cut it on the sodick out of 0.1mm thick copper shim stock. 

![The sketch](sketch.png)

It was quite difficult to cut because the material is quite thin. We fixtured it the best we could across a corner of the base, but this still left a significant unsupported span that could be flexed easily by hand. 

![Part in the EDM](edm.jpg)

When we tried to cut it, we found that the machine would cut, but the wire would always break after a few millimeters. We think this is because the water jet around the wire vibrated the copper plate, causing it to contact the wire and break it. 

![Resultant cut](cut.jpg)
![Locations of wire breaks](breaks.jpg)

It may be possible to eliminate the vibration by sandwitching the copper sheet between the sacrificial thicker plates of metal or slamping many copper sheets together, but a different fabrication technology is probably better suited to the task. 