Today we tried to cut the connector leadframes using the Fablight and the Trotec speedy 100 flexx. 

The fablight worked extremely well. This is the resultant part cut out of 0.1mm thick copper sheet:

![Fablight part](fablight_part.png)

We just used the default settings for this thickness of copper and got good results. The top surface turned out perfect, with a very small kerf, indicating that this technology is probably the best way to make the connectors. Here is a picture under the microscope:

![Top surface](top_microscope.jpg)

The bottom surface was much rougher, mostly due to pitting and buildup of slag from the laser cutting process. However, there are some steps that can be taken to either cleanup the copper after cutting (such as running it under an orbital sander) or reduce the buildup of slag in the first place (such as layering two sheets of copper, cutting them as one sheet with double the thickness and then only using the parts from the top layer). 

![Bottom surface](bottom_microscope.jpg)

Next we tried to make the same part on the Trotec. Having a significantly lower power than the fablight, it struggled to cut through the copper even after 20 minutes of repeated passes. This machine is better suited to etching the thin copper foil layers on PCBs, or very thin sheet metal for SMD stencils. \

Here is the result after 20 minutes of cutting:

![Trotec cut](trotec_cut.jpg)